
var fm = jQuery.noConflict();

fm(document).ready(function() {
  fm("#file-manager-container .file-container.folder").dblclick(function() {
    // @TODO: Make this url from Drupal settings.
    window.location.href = "/file_manager/folders/" + fm(this).attr("data-uri");
  });
  fm("#fm-actions-container .fm-view-toggle .list").click(function() {
    if (fm("#fm-list-grid-view").hasClass("fm-grid")) {
      fm("#fm-list-grid-view").toggleClass("fm-grid fm-list");
    }
  });
  fm("#fm-actions-container .fm-view-toggle .grid").click(function() {
    if (fm("#fm-list-grid-view").hasClass("fm-list")) {
      fm("#fm-list-grid-view").toggleClass("fm-list fm-grid");
    }
  });
  fm("#fm-actions-container .fm-actions .delete").click(function() {
    if (confirm(Drupal.t("Are you sure you want to delete the file/folder?"))) {
      var fid = "";
      var uri = "";
      if (fm("#file-manager-container .file-container.active").hasClass("folder")) {
        fid = fm("#file-manager-container .file-container.folder.active").attr("data-id");
        url = "/file_manager/delete-folder";
      }
      else if (fm("#file-manager-container .file-container.active").hasClass("file")) {
        fid = fm("#file-manager-container .file-container.file.active").attr("data-id");
        url = "/file_manager/delete-file";
      }
      if (fid != "") {
        file_manager_delete_files(fid, url);
      }
    }
    else {
      return false;
    }
  });
  fm("#file-manager-container .file-container").click(function() {
    fm('#file-manager-container .file-container').removeClass('active');
    fm(this).addClass('active');
  });
});

function file_manager_delete_files(fid, url) {
  fm.ajax({
    type: "POST",
    url: url,
    data: "fid=" + fid,
    success: function(data){
      if (data == 'success') {
        window.location.href = window.location.href;
      }
      else if (data == 'error') {
        window.location.href = window.location.href;
      }
      else {
        alert(Drupal.t("Error occured while deleting the file/folder."));
      }
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      if (XMLHttpRequest.status == 403) {
        alert(Drupal.t("You don't have sufficient permissions to delete this file/folder."));
      }
      else {
        alert(Drupal.t("Error occured while deleting the file/folder."));
      }
    }
  });
}
