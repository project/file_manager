<?php

/**
 * @file
 * File manager administartor settings.
 */

/**
 * Access callback for file manager settings.
 */
function file_manager_settings_form() {
  $form['file_manager_folder_path'] = array(
    '#type' => 'textfield',
    '#title' => t('File manager folder path'),
    '#default_value' => variable_get('file_manager_folder_path', ''),
    '#required' => TRUE,
    '#maxlength' => 255,
    '#description' => t('A local file system path from where the file manager looks. This directory must be relative to the Drupal installation directory. If directory doesn\'t exits, then it will create the directory. <br>Ex: <strong>sites/default/files</strong>'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Validation for file manager settings.
 */
function file_manager_settings_form_validate($form, &$form_state) {
  $directory = $form_state['values']['file_manager_folder_path'];

  if (!is_dir($directory) && !drupal_mkdir($directory, NULL, TRUE)) {
    // If the directory does not exists and cannot be created.
    form_set_error('file_manager_folder_path', t('The directory %directory does not exist and could not be created.', array('%directory' => $directory)));
    watchdog('file system', 'The directory %directory does not exist and could not be created.', array('%directory' => $directory), WATCHDOG_ERROR);
  }

  if (is_dir($directory) && !is_writable($directory) && !drupal_chmod($directory)) {
    // If the directory is not writable and cannot be made so.
    form_set_error('file_manager_folder_path', t('The directory %directory exists but is not writable and could not be made writable.', array('%directory' => $directory)));
    watchdog('file system', 'The directory %directory exists but is not writable and could not be made writable.', array('%directory' => $directory), WATCHDOG_ERROR);
  }
}

/**
 * Submit handler for file manager settings.
 */
function file_manager_settings_form_submit($form, &$form_state) {
  form_state_values_clean($form_state);
  foreach ($form_state['values'] as $key => $value) {
    variable_set($key, $value);
  }
  drupal_set_message(t('The configuration options have been saved.'));
}
