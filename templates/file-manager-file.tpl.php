<div class="file-container <?php print $type; ?>" data-id="<?php print $path; ?>" data-uri="<?php print $uri; ?>">
  <?php if ($type == 'folder'): ?>
  <div class="item folder">
    <div class="fm-thumbnail">icon</div>
    <div class="name"><span title="<?php print $name; ?>"><?php print $name; ?></span></div>
    <div class="type"><?php print t('File folder'); ?></div>
    <div class="size">-</div>
  </div>
  <?php else: ?>
  <div class="item <?php print $extension; ?>">
    <div class="fm-thumbnail" style="background: url('/<?php print $path; ?>')">icon</div>
    <div class="name"><span title="<?php print $name; ?>"><?php print $name; ?></span></div>
    <div class="type"><?php print $extension; ?></div>
    <div class="size"><?php print format_size($size); ?></div>
  </div>
  <?php endif; ?>
</div>