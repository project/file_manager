<div id="file-manager-container" class="file-manager-container">
  <div id ="fm-actions-container" class="fm-actions-container">
    <div class="fm-actions"><span class="delete">Delete</span></div>
    <div class="fm-view-toggle"><span class="list">List</span><span class="grid">Grid</span></div>
  </div>
  <div id="fm-list-grid-view" class="fm-grid">
    <?php print $files;?>
  </div>
</div>
