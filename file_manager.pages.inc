<?php

/**
 * @file
 * File manager callback functions.
 */

/**
 * Access callback for delete file.
 */
function file_manager_delete_file_callback() {
  $fid = filter_xss($_POST['fid']);
  $filename = end(explode("/", $fid));
  $delete = file_unmanaged_delete($fid);
  if ($delete) {
    drupal_set_message(t("File <strong>@name</strong> has been deleted.", array('@name' => $filename)));
    echo "success";
  }
  else {
    drupal_set_message(t("Error occured while deleting the file <strong>@name</strong>.", array('@name' => $filename)));
    echo "error";
  }
  drupal_exit();
}

/**
 * Access callback for delete file.
 */
function file_manager_delete_folder_callback() {
  $fid = filter_xss($_POST['fid']);
  $fid = rtrim($fid, "/");
  $filename = end(explode("/", $fid));
  $delete = file_unmanaged_delete_recursive($fid);
  if ($delete) {
    drupal_set_message(t("Folder <strong>@name</strong> has been deleted.", array('@name' => $filename)));
    echo "success";
  }
  else {
    drupal_set_message(t("Error occured while deleting the folder <strong>@name</strong>.", array('@name' => $filename)));
    echo "error";
  }
  drupal_exit();
}
